package com.company;

import java.util.ArrayList;

public class Player {

    private DeckOfCards deckOfCards = new DeckOfCards(0);

    private String name;

    public Player(String name){ this.name = name; }

    public void addCardToDeck(Card card){ deckOfCards.addCard(card); }

    public Card removeCard(){ return deckOfCards.removeCard(); }

    public Card getCard(){ return deckOfCards.getCard();}

    public boolean deckIsEmpty(){ return deckOfCards.isEmpty();}

    public String toString(){ return this.name; }

    public void addCardsToDeck(ArrayList<Card> cards){
        for (Card c: cards)
            this.addCardToDeck(c);
    }
}
