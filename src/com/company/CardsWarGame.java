package com.company;

import java.util.ArrayList;

public class CardsWarGame
{
   private DeckOfCards deckOfCards =  new DeckOfCards(52);

   private Player[] players = new Player[2];

   private ArrayList<Card> turnCards = new ArrayList<Card>();

   public CardsWarGame(){

      for (int i = 0; i < players.length; i++)
         players[i] = new Player("player" + i);

      deckOfCards.shuffle();
      divideDeck();
   }

   private void divideDeck(){
      for (int i = 0; i < deckOfCards.size(); i++ ) {
         players[i % players.length].addCardToDeck(deckOfCards.getCard());
         //deckOfCards.removeCard();
      }
   }

   private boolean isWar(){

      if (players[0].getCard().equals(players[1].getCard()))
         return true;
      return false;
   }

   private void war(){
      for (int i = 0; i<2 ; i++)
         addToTurnCards();
   }

   private void addToTurnCards() {
      for (Player p : players)
         turnCards.add(p.removeCard());
   }

   public void turn() {

      if (isWar())
         war();

      System.out.println(players[0].getCard());
      System.out.println(players[1].getCard());

      if (players[0].getCard().isGreaterThan(players[1].getCard()))
         players[0].addCardsToDeck(turnCards);

      else
         players[1].addCardsToDeck(turnCards);

      turnCards.clear();
   }

   public void run(){

      Player winner;

      while (!players[0].deckIsEmpty() || !players[1].deckIsEmpty() )
         this.turn();

      if (!players[0].deckIsEmpty())
         winner = players[0];
      else
         winner = players[1];
   }
}
