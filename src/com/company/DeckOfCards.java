package com.company;
import java.util.ArrayList;
import java.util.Collections;


public class DeckOfCards
{

   private String[] faces = { "Ace", "Deuce", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King" };

   private String[] suits = { "Hearts", "Diamonds", "Clubs", "Spades" };

   private ArrayList<Card> deck = new ArrayList();

   private int currentCard = 0;


   public DeckOfCards(int number_of_cards) {

      for ( int count = 0; count < number_of_cards; count++ )
         deck.add(new Card(faces[ count % 13 ], suits[ count / 13 ] ));
   }

   public void shuffle() {
      Collections.shuffle(deck);
   }

   public void addCard(Card card){ deck.add(card); }

   public boolean isEmpty(){ return deck.size() == 0; }

   public int size(){
      return deck.size();
   }

   public Card removeCard()
   {
      if ( currentCard < deck.size() )
         return deck.remove(currentCard++);

      return null;
   }

   public Card getCard()
   {
      if ( currentCard < deck.size() )
         return deck.get(currentCard++);

      return null;
   }

}

