package com.company;
// Card class represents a playing card.

enum Face {
   Ace, Deuce, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King
}
enum Suit{
   Hearts, Diamonds, Clubs, Spades
}

public class Card 
{
   private Face face;

   private Suit suit;

   // two-argument constructor initializes card's face and suit
   public Card( String cardFace, String cardSuit ) {

      face = Face.valueOf(cardFace);
      suit = Suit.valueOf(cardSuit);
   }

   public boolean isGreaterThan(Card other) { return this.face.ordinal() > other.face.ordinal(); }

   public boolean equals(Card other) { return this.face.equals(other.face); }

   public String toString() {  return face.toString() + " of " + suit.toString(); }
}
